let tabs = document.getElementById('tabs');
let tabsContent = document.getElementById('tabs-content');


tabs.onclick = (event) => {
	tabs.querySelector(".active").classList.toggle("active");
    tabsContent.querySelector("li:not([hidden])").hidden = true;
    
	event.target.classList.toggle("active");
	tabsContent.children[event.target.dataset.index].hidden = false;
}
 

for (let i = 0; i < tabsContent.children.length; i++){
	tabs.children[i].dataset.index = i;
	if(i)
    tabsContent.children[i].hidden = true;
}
    